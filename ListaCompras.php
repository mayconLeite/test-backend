<?php

class ListaCompras
{
    private $items;

    public function __construct(array $items) {
        $this->items = $items;
        $this->ordination();
    }

    public function ordination() {
        uksort($this->items, function ($a, $b) {
            $mes = [
                'janeiro',
                'fevereiro',
                'março',
                'abril',
                'maio',
                'junho',
                'julho',
                'agosto',
                'setembro',
                'outubro',
                'novembro',
                'dezembro'
            ];

            $key1 = array_search(strtolower($a), $mes);
            $key2 = array_search(strtolower($b), $mes);

            if ($key1 == $key2) {
                return 0;
            }
            
            return ($key1 > $key2) ? 1 : -1;
        });

        foreach ($this->items as &$item) {
            ksort($item, SORT_NATURAL);

            foreach ($item as &$produto) {
                uasort($produto, function($a, $b) {
                    if ($a == $b) {
                        return 0;
                    }

                    return ($a < $b) ? 1 : -1;
                });
            }
        }
    }

    private function Ajuste($texto) {
        $texto = strtolower(str_replace('_', ' ', $texto));

        $correctWords = [
            'hignico' => 'higiênico',
            'brocolis' => 'brócolis',
            'sabao em po' => 'sabão em pó',
            'chocolate ao leit' => 'Chocolate ao leite'
        ];

        $texto = str_replace(array_keys($correctWords), $correctWords, $texto);
        $texto = ucfirst($texto);

        return $texto;
    }

    private function walkList($callback) {
        foreach ($this->items as $mes => $categoria) {
            foreach ($categoria as $categoria => $produto) {
                foreach ($produto as $produto => $quantidade) {
                    $callback($this->ajuste($mes), $this->ajuste($categoria), $this->ajuste($produto), $quantidade);
                }
            }
        }
    }

    public function generCSV(string $filename) {
        $filecsv = fopen($filename, 'w+');    
        fputcsv($filecsv, array('Mês', 'Categoria', 'Produto', 'Quantidade'));

        $this->walkList(function($mes, $categoria, $produto, $quantidade) use ($filecsv) {
            fputcsv($filecsv, array($mes, $categoria, $produto, $quantidade));
        });       
    }

    public function save() {
        $db = new PDO('mysql:host=db;dbname=compras', 'root', '');

        $this->walkList(function($mes, $produto, $id_categoria, $quantidade) use ($db) {
            $stmt = $db->prepare("INSERT INTO lista_compra('mes','produto' 'id_categoria', 'quantidade') VALUES (?, ?, ?, ?)");
            $stmt->bindParam(1, $mes);
            $stmt->bindParam(2, $categoria);
            $stmt->bindParam(3, $produto);
            $stmt->bindParam(4, $quantidade);
            $stmt->execute();
        });
    }

}