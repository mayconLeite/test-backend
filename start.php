<?php
require 'ListaCompras.php';

$lista_compra = new ListaCompras(include('lista-de-compras.php'));

$test = (count($argv) > 1) ? $argv[1] : 'csv'; 

if ($test == 'mysql' || $test == 'all') {
    echo "\nGerando Mysql\n";

    $lista_compra->save();
}

if ($test !== 'mysql') {
    echo "\nGerando CSV\n";

    $lista_compra->generCSV('lista-compras-ano.csv');
    $lista_compra->save();
}
